# colo

A Vim 8+ package containing the colourschemes I like.

## Installation

To install `colo` clone [this](https://git.envs.net/duriny/colo) repo under
`~/.vim/pack/`:

```shell
# clone the package
git clone https://git.envs.net/duriny/colo ~/.vim/pack/colo

# initialize all the submodules
cd ~/.vim/pack/colo
git submodule update --init --recursive
```

...or, if you store your Vim configuration it git, you can add `colo` as a
submodule:

```shell
# add the package as a submodule
git submodule add --branch main https://git.envs.net/duriny/colo ~/.vim/pack/colo

# initialize the package submodule (and it's own submodules)
git submodule update --init --recursive

# commit the new package
git commit -m "Added colo package" ~/.vim/pack/colo
```

## Updating

If you cloned `colo` on its own, just `cd ~/.vim/pack/colo && git pull` to
update the package. Otherwise, if you added `colo` as a submodule, just run:

```shell
# update the colourschemes
git submodule update --recursive --remote ~/.vim/pack/colo

# commit the updates
git commit -m "Updated colo" ~/.vim/pack/colo
```

## Usage

Once you have `colo` installed, you can use any of the colourscheme included,
just add the following to your `~/.vimrc` file, replacing `{name}` with the name
of the colourscheme you want to use.

```vim
packadd {name}
colo    {name}

" e.g. for gruvbox
packadd gruvbox8
colo    gruvbox8
```

## Hacking

### Why is {colourscheme} missing?

I probably haven't seen it yet, send a PR if you want it added, or even better,
fork `colo`, centralisation is bad!

### How do I add {colourscheme}?

Add the new colourscheme as a submodule of `colo`, replace `{branch}` with the
main/master branch name of the upstream source, `{repo}` with the upstream clone
path, and `{colourscheme}` with the name of the colourscheme.

```shell
git submodule add --branch "{branch}" "{repo}" "opt/{colourscheme}"
git commit -am "Add {colourscheme}"
```

### Why do I have to call `packadd {colourscheme}`?

It is only possible to use one colourschemes at a time, so it is not necessary
load the entire package worth of colourschemes, so they are stored as optional
under `pack/colo/opt`, rather than `pack/colo/start`. If you want a few of your
favorite colourschemes to be always available, move them to the start directory,
`pack/colo/start`:

```shell
git mv "opt/{colourscheme}" "start/{colourscheme}"
git commit -am "Autoload {colourscheme}"
```

## Licensing

This package is distributed under the terms of the [Unlicense](LICENSE), and as
such is released into the public domain.

Note: *Each colourscheme is licensed by its creator, this repo only references
them, see the upstream source for licensing terms.*
